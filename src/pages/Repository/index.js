import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Proptypes from 'prop-types';
import api from '../../services/api';
import Container from '../../components/Container';
import { Loading, Owner, IssueList, Navgroup, IssuesButton } from './styles';

class Repository extends Component {
  static propTypes = {
    match: Proptypes.shape({
      params: Proptypes.shape({
        repository: Proptypes.string,
      }),
    }).isRequired,
  };

  state = {
    repository: {},
    issues: [],
    page: 1,
    issueState: 'all',
    loading: true,
    errorString: '',
    lastpage: false,
  };

  async componentDidMount() {
    const { match } = this.props;
    const repoName = decodeURIComponent(match.params.repo);
    try {
      const [repository, issues] = await Promise.all([
        api.get(`/repos/${repoName}`),
        api.get(`/repos/${repoName}/issues`, {
          params: {
            state: 'all',
            per_page: 5,
            page: 1,
          },
        }),
      ]);
      this.setState({
        repository: repository.data,
        issues: issues.data,
        loading: false,
      });
    } catch (err) {
      console.log(String(err));
      this.setState({
        errorString: String(err),
      });
    }
  }

  pageViewButton = async e => {
    const { match } = this.props;
    const repoName = decodeURIComponent(match.params.repo);
    const { page, issueState } = this.state;
    if (!this.state.lastpage || (page === 1 && e === 'next')) {
      const newPage = page + (e === 'next' ? +1 : -1);
      const response = await api.get(`/repos/${repoName}/issues`, {
        params: {
          state: issueState,
          per_page: 5,
          page: newPage,
        },
      });
      if (!response) {
        this.setState({
          lastpage: true,
          page: page - 1,
        });
      } else {
        this.setState({
          issues: response.data,
          page: newPage,
        });
      }
    }
  };

  statusButton = async e => {
    const { match } = this.props;
    const repoName = decodeURIComponent(match.params.repo);
    const { page, issueState } = this.state;
    if (issueState !== e) {
      const response = await api.get(`/repos/${repoName}/issues`, {
        params: {
          state: e,
          per_page: 5,
          page,
        },
      });
      this.setState({ issues: response.data, issueState: e });
    }
  };

  render() {
    const { repository, issues, loading } = this.state;
    if (loading) {
      return <Loading>Carregando</Loading>;
    }
    return (
      <Container>
        <Owner>
          <Link to="/">Voltar aos repositorios</Link>
          <img src={repository.owner.avatar_url} alt={repository.owner.login} />
          <h1>{repository.name}</h1>
          <p>{repository.description}</p>
        </Owner>
        <IssueList>
          {issues &&
            issues.map(issue => (
              <li key={String(issue.id)}>
                <img src={issue.user.avatar_url} alt={issue.user.login} />
                <div>
                  <strong>
                    <a href={issue.html_url}>{issue.title}</a>
                    {issue.labels.map(label => (
                      <span key={String(label.id)}>{label.name}</span>
                    ))}
                  </strong>
                  <p>{issue.user.login}</p>
                </div>
              </li>
            ))}
        </IssueList>
        <Navgroup>
          <IssuesButton value="prev" onClick={() => this.pageViewButton()}>
            {'<'}
          </IssuesButton>
          <IssuesButton value="all" onClick={() => this.statusButton('todas')}>
            todos
          </IssuesButton>
          <IssuesButton value="open" onClick={() => this.statusButton('open')}>
            abertas
          </IssuesButton>
          <IssuesButton
            value="close"
            onClick={() => this.statusButton('closed')}
          >
            fechadas
          </IssuesButton>
          <IssuesButton
            value="next"
            onClick={() => this.pageViewButton('next')}
          >
            >
          </IssuesButton>
        </Navgroup>
      </Container>
    );
  }
}

export default Repository;
